<?php
class RytGQPostAppearance {
    public function set_css() {
        wp_enqueue_style(
            ryt_gq_get_name(),
            ryt_gq_get_assets_dir_url() . 'css/' . ryt_gq_get_name() . '.css',
            array(), '0.1.4'
        );
    }

    public function enqueue_script() {
        wp_enqueue_script(ryt_gq_get_name() . '-instagram', '//www.instagram.com/embed.js', array(), '', true);
        wp_enqueue_script(ryt_gq_get_name() . '-twitter', '//platform.twitter.com/widgets.js', array(), '', true);
    }

    function addasync_enqueue_script($tag, $handle) {
        if (ryt_gq_get_name() . '-twitter' !== $handle && ryt_gq_get_name() . '-instagram' !== $handle) {
            return $tag;
        }
        return str_replace(' src', ' async="async" src', $tag);
    }
}