<?php
class RytGQActions {
    public function __construct() {
        $this->add_actions();
    }

    private function add_actions() {
        add_action('admin_enqueue_scripts', array(ryt_gq_get_instance('AdminMenu'), 'enqueue_script'));
        add_action('admin_menu', array(ryt_gq_get_instance('AdminMenu'), 'admin_menu'));
        add_action('admin_init', array(ryt_gq_get_instance('AdminMenu'), 'admin_init'));
        add_action('rest_api_init', array(ryt_gq_get_instance('CallbackInstagram'), 'rest_api_init'));
        add_action('add_option', array(ryt_gq_get_instance('AdminMenu'), 'add_option'), 10, 3);
        add_action('update_option', array(ryt_gq_get_instance('AdminMenu'), 'check_updated'), 10, 3);
        add_action('wp_head', array(ryt_gq_get_instance('PostAppearance'), 'set_css'), 1);
        add_action('wp_enqueue_scripts', array(ryt_gq_get_instance('PostAppearance'), 'enqueue_script'));
        add_action(ryt_gq_get_name() . '_update_schedules', array(ryt_gq_get_instance('Cron'), 'update_schedules'));
        ryt_gq_get_instance('Cron')->add_schedule_action();
    }
}
