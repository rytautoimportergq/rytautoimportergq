<?php
function ryt_gq_get_name() {
    return 'ryt-autoimporter-gq';
}

function ryt_gq_get_name_jp() {
    return 'RYT オートインポーター GQ';
}

function ryt_gq_get_sections() {
    return array('Rss', 'Youtube', 'Twitter', 'Instagram');
}

function ryt_gq_get_current_date() {
    $now = new DateTime('', new DateTimeZone('Asia/Tokyo'));
    return $now->format('Y年n月j日 G時i分');
}

function ryt_gq_get_history_table() {
    global $wpdb;
    return $wpdb->prefix . 'ryt_gq_history';
}

function ryt_gq_get_plugin_dir() {
    return dirname(__FILE__) . '/';
}

function ryt_gq_get_plugin_file() {
    return ryt_gq_get_plugin_dir() . ryt_gq_get_name() . '.php';
}

function ryt_gq_get_plugin_basename() {
    return plugin_basename(dirname(__FILE__) . '/' . ryt_gq_get_name() . '.php');
}

function ryt_gq_get_assets_dir() {
    return ryt_gq_get_plugin_dir() . 'assets/';
}

function ryt_gq_get_assets_dir_url() {
    return plugin_dir_url(ryt_gq_get_plugin_file()) . 'assets/';
}

function ryt_gq_get_redirect_base() {
    $base = get_option("ryt_gq_redirect_base");
    if (substr($base, -1, 1) != '/') {
        $base .= '/';
    }
    return $base;
}

function ryt_gq_get_instance($key_name) {
    global $RYT_CLASSES;
    return $RYT_CLASSES['RytGQ' . $key_name];
}

function ryt_gq_get_html($url) {
    $options = array(
        'http'  => array(
            'method'=> 'GET',
            'header'=> 'Accept-language: ja-JP'
        )
    );
    $context = stream_context_create($options);
    return file_get_contents($url, false, $context);
}