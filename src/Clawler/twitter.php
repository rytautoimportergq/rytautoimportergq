<?php
use Abraham\TwitterOAuth\TwitterOAuth;

class RytGQTwitter {
    public function __construct() {
        $this->section = 'Twitter';
    }

    public function scheduled_action($key) {
        $pm = ryt_gq_get_instance('PostManager');

        $fetch_items = get_option($this->section . '_fetch_items', '');
        $fetch_items = ($fetch_items != '') ? json_decode($fetch_items, true) : array();

        foreach ($fetch_items as $item) {
            if ($item['key'] === $key) {
                $fetch_item = (object)$item['val'];

                $keyword = $fetch_item->var;
                $title = $fetch_item->title;
                $title = str_replace('{{keyword}}', $keyword, $title);
                $title = str_replace('{{date}}', ryt_gq_get_current_date(), $title);
                $pm->initialize($this->section, $title, $fetch_item);

                $result = $this->query_twitter($keyword);
                foreach($result as $tweet) {
                    if ($pm->count() > 9) {
                        break;
                    }
                    $url = $tweet->url;
                    $post_item = $pm->add($url);
                    if (!is_null($post_item)) {
                        $post_item->title = explode("\n", mb_substr(strip_tags($tweet->content), 0, 15))[0];
                        $post_item->description = "
                            <blockquote class='twitter-tweet' data-lang='ja'>
                                <p lang='ja' dir='ltr'>$tweet->content</p>
                                &mdash; $tweet->user_name ($tweet->user_id)
                                <a href='$url'>$tweet->timestamp</a>
                            </blockquote>
                        ";
                        $post_item->image = null;
                    }
                }
                $pm->post();
                return;
            }
        }
    }

    private function query_twitter($keyword) {
        require_once(ryt_gq_get_plugin_dir() . 'include/phpQuery-onefile.php');
        $html = ryt_gq_get_html('https://mobile.twitter.com/search?q=' . urlencode($keyword) . '&src=typed_query');
        $doc = phpQuery::newDocument($html, 'text/html');

        $result = array();
        $tw_table = $doc['table.tweet'];
        $len = count($tw_table->elements);
        for ($i = 0; $i < $len; $i++) {
            $tweet = $doc['table.tweet:eq(' . $i . ')'];
            $result[] = (object)array(
                'url' => 'https://twitter.com' . str_replace('?p=v', '', $tweet->attr('href')),
                'user_id' => $tweet->find('.username')->text(),
                'user_name' => trim($tweet->find('td.avatar')->find('a')->find('img')->attr('alt')),
                'content' => $tweet->find('div.dir-ltr')->html(),
                'timestamp' => $tweet->find('td.timestamp')->find('a')->text()
            );
        }
        return $result;
    }
}
