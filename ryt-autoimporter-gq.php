<?php
/**
 * Plugin Name:     RYT オートインポーター GQ
 * Description:     RSS, SNS(Instagram, Twitter, Youtube) を自動的に記事として取り込みます。
 * Author:          RYT
 * Text Domain:     ryt-aoutimporter-gq
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         RYT_Autoimporter_GQ
 */

global $RYT_GQ_CONFIG;
global $RYT_CLASSES;

require_once dirname(__FILE__) . '/helper.php';
require_once dirname(__FILE__) . '/loader.php';

$ryt_gq_loader = new RytGQLoader();
$ryt_gq_loader->load($RytGQ_CONFIG);
