<?php
class RytGQLoader {
    public function load(&$config){
        global $RYT_CLASSES;
        $RYT_CLASSES = array();

        $base = dirname(__FILE__) . '/';

        // load config
        $config = array();
        foreach ( glob($base . 'config/*') as $file) {
            if (is_file($file)) {
                $config[basename($file, '.json')] = json_decode(file_get_contents($file), true);
            }
        }
        
        // require all libraries
        $this->all_require($base . 'include');
        $this->all_load($base . 'vendor');
        $this->all_require_and_new($base . 'src');
        require_once $base . 'hooks.php';
        $RYT_CLASSES['hooks'] = new RytGQHooks;
        require_once $base . 'actions.php';
        $RYT_CLASSES['actions'] = new RytGQActions;
        require_once $base . 'filters.php';
        $RYT_CLASSES['filters'] = new RytGQFilters;

        // set pluggable
        require_once(ABSPATH . "wp-includes/pluggable.php" );
    }

    private function all_load($path) {
        foreach ( glob($path . '/*') as $item) {
            if (is_dir($item)) {
                require_once $item . '/autoload.php';
            }
        }
    }

    private function all_require($path) {
        foreach ( glob($path . '/*') as $item) {
            if (is_file($item)) {
                require_once $item;
            } else {
                $this->all_require($item);
            }
        }
    }

    private function all_require_and_new($path) {
        foreach ( glob($path . '/*') as $item) {
            if (is_file($item)) {
                $this->require_and_new($item);
            } else {
                $this->all_require_and_new($item);
            }
        }
    }

    private function require_and_new($item) {
        global $RYT_CLASSES;

        require_once $item;
        $class_name = strtolower($item);
        $class_name = basename($class_name, '.php');
        $class_name = str_replace('_', ' ', $class_name);
        $class_name = ucwords($class_name);
        $class_name = str_replace(' ', '', $class_name);
        $class_name = 'RytGQ' . $class_name;
        $RYT_CLASSES[$class_name] = new $class_name;
    }
}
