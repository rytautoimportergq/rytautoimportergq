<?php
class RytGQAdminMenu {
    public function admin_menu() {
        add_options_page(
            esc_html__(ryt_gq_get_name_jp()),
            esc_html__(ryt_gq_get_name_jp()),
            'manage_options',
            ryt_gq_get_name(),
            array( $this, 'show_admin_options' )
        );
    }

    public function admin_init() {
        register_setting(ryt_gq_get_name(), ryt_gq_get_name() . '_updated');
        foreach(ryt_gq_get_sections() as $section) {
            register_setting(ryt_gq_get_name(), $section . '_fetch_items');
        }
    }

    public function enqueue_script() {
        wp_enqueue_style('wp-jquery-ui-dialog');
        wp_enqueue_style(ryt_gq_get_name(), ryt_gq_get_assets_dir_url() . 'css/' . ryt_gq_get_name() . '.css', array(), '0.1.20');

        wp_enqueue_script('jquery-ui-dialog');
        wp_enqueue_media();
        wp_enqueue_script(ryt_gq_get_name() . '_sha256', ryt_gq_get_assets_dir_url() . 'js/sha256.js', array(), '0.9.0');
        wp_enqueue_script(ryt_gq_get_name() . '_admin', ryt_gq_get_assets_dir_url() . 'js/admin_options.js', array('jquery'), '0.2.14');
    }

    public function plugin_action_links($links, $file) {
        if ($file == ryt_gq_get_plugin_basename()) {
            $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=' . ryt_gq_get_name() . '">設定</a>';
            array_unshift($links, $settings_link);
        }

        return $links;
    }

    public function show_admin_options() {
        ryt_gq_get_instance('AdminShowOptions')->show();
    }

    public function add_option($option_name, $value) {
        $this->update_option($option_name, $value);
    }

    public function check_updated($option_name, $old, $new) {
        $this->update_option($option_name, $new);
    }

    private function update_option($option_name, $new) {
        if ($option_name == ryt_gq_get_name() . '_updated') {
            $updates = json_decode($new, true);
            foreach($updates['value'] as $section => $value) {
                ryt_gq_get_instance('Cron')->update_schedules($section);
            }
        }
    }
}