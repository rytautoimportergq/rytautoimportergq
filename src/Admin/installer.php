<?php
class RytGQInstaller {
    public function install() {
        ryt_gq_get_instance('DbManager')->setup_db();
    }

    public function clear_settings() {
        $cron_settings = get_option('cron', '');
        foreach ($cron_settings as $jobs) {
            foreach ($jobs as $job => $info) {
                if (strpos($job, ryt_gq_get_name() . '_') !== false) {
                    $jz = explode('_', $job);
                    wp_clear_scheduled_hook($job, array($jz[2]));
                }
            }
        }

        foreach(ryt_gq_get_sections() as $section) {
            $fetch_items = get_option($section . '_fetch_items', '');
            $items = json_decode($fetch_items);
            foreach ((array)$items as $item) {
                $item->val->int = '-1';
            }
            update_option($section . '_fetch_items', json_encode($items));
        }
    }

    public function uninstall() {
        ryt_gq_get_instance('DbManager')->delete_db();
    }
}