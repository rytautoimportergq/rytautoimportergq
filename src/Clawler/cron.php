<?php
class RytGQCron {
    public function cron_schedules($schedules) {
        $schedules['10min'] = array(
            'interval' => 10*60,
            'display' => __( 'Once every 10 minutes' )
        );
        return $schedules;  
    }

    public function update_schedules($section) {
        // list all schedule
        $cron_settings = get_option('cron', '');
        $saved_jobs = array();
        foreach ($cron_settings as $jobs) {
            if (is_array($jobs)) {
                foreach ($jobs as $job => $info) {
                    if (strpos($job, ryt_gq_get_name() . '_' . $section) !== false) {
                        $saved_jobs[] = $job;
                    }
                }
            }
        }
        // schedule crons
        $fetch_items = isset($_POST[$section . '_fetch_items']) ? $_POST[$section . '_fetch_items'] : '';
        $items = json_decode(str_replace('\\', '', $fetch_items));
        $updated = array();
        foreach ((array)$items as $item) {
            $event_name = ryt_gq_get_name() . '_' . $section . '_' . $item->key;
            if (in_array($event_name, $saved_jobs)) {
                $schedule = wp_get_schedule($event_name);
                if ($item->val->int != $schedule) {
                    wp_clear_scheduled_hook($event_name, array($item->key));
                }
                $updated[] = $event_name;
            }
            if ($item->val->int != -1) {
                wp_schedule_event(time() + 5, $item->val->int, $event_name, array($item->key));
            }
        }
        foreach ($saved_jobs as $job) {
            if (!in_array($job, $updated)) {
                $jz = explode('_', $job);
                wp_clear_scheduled_hook($job, array($jz[2]));
            }
        }
    }

    public function add_schedule_action() {
        foreach(ryt_gq_get_sections() as $section) {
            $fetch_items = get_option($section . '_fetch_items', '');
            $items = json_decode($fetch_items);
            foreach ((array)$items as $item) {
                $event_name = ryt_gq_get_name() . '_' . $section . '_' . $item->key;
                add_action($event_name, array(ryt_gq_get_instance($section), 'scheduled_action'), 10, 1);
            }
        }
    }
}
