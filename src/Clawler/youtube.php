<?php
class RytGQYoutube {
    public function __construct() {
        $this->section = 'Youtube';
    }

    public function scheduled_action($key) {
        $pm = ryt_gq_get_instance('PostManager');

        $fetch_items = get_option($this->section . '_fetch_items', '');
        $fetch_items = ($fetch_items != '') ? json_decode($fetch_items, true) : array();

        foreach ($fetch_items as $item) {
            if ($item['key'] === $key) {
                $fetch_item = (object)$item['val'];
                $keyword = $fetch_item->var;
                $title = $fetch_item->title;
                $title = str_replace('{{keyword}}', $keyword, $title);
                $title = str_replace('{{date}}', ryt_gq_get_current_date(), $title);
                $pm->initialize($this->section, $title, $fetch_item);

                $result = $this->query_youtube($keyword);
                foreach($result as $video) {
                    if ($pm->count() > 9) {
                        break;
                    }
                    $id = $video->id;
                    $url = "https://www.youtube.com/$id";
                    $post_item = $pm->add($url);
                    if (!is_null($post_item)) {
                        $post_item->title = $video->title;
                        $post_item->description = "
                            <iframe width='560' height='315' class='youtube-frame'
                                src='https://www.youtube.com/embed/$id'
                                frameborder='0' allow='autoplay; encrypted-media' allowfullscreen>
                            </iframe>
                            <p>$video->description</p>
                        ";
                    }
                }
                $pm->post();
                return;
            }
        }
    }

    private function query_youtube($keyword) {
        require_once(ryt_gq_get_plugin_dir() . 'include/phpQuery-onefile.php');
        $html = ryt_gq_get_html('https://www.youtube.com/results?sp=CAJCBAgBEgA%253D&search_query=' . urlencode($keyword));
        $doc = phpQuery::newDocument($html, 'text/html');

        $result = array();
        $video_list = $doc['div.yt-lockup-content'];
        $len = count($video_list->elements);
        for ($i = 0; $i < $len; $i++) {
            $video = $doc['div.yt-lockup-content:eq(' . $i . ')'];
            $title = $video->find('h3.yt-lockup-title');
            $result[] = (object)array(
                'id' => str_replace('/watch?v=', '', $title->find('a')->attr('href')),
                'title' => $title->text(),
                'description' => $video->find('.yt-lockup-description')->html(),
                'meta' => $video->find('.yt-lockup-meta-info')->find('li:eq(0)')->text() . 'の投稿 - ' . $video->find('.yt-lockup-meta-info')->find('li:eq(1)')->text()
            );
        }
        return $result;
    }
}
