<?php
class RytGQAdminShowOptions {
    public function show() {
        $tab = ryt_gq_get_instance('AdminShowOptionsTab');
?>
            <div class='wrap'>
                <h1><?= ryt_gq_get_name_jp() ?></h1>

                <h2 class='title'>インポート設定</h2>
                <div class='<?= ryt_gq_get_name() ?>-tabs'>
                    <input id='rss' type='radio' name='tab_item' data-section='Rss' checked>
                    <label class='tab_item' for='rss'>Rss</label>
                    <input id='youtube' type='radio' name='tab_item' data-section='Youtube'>
                    <label class='tab_item' for='youtube'>Youtube</label>
                    <input id='twitter' type='radio' name='tab_item' data-section='Twitter'>
                    <label class='tab_item' for='twitter'>Twitter</label>
                    <input id='instagram' type='radio' name='tab_item' data-section='Instagram'>
                    <label class='tab_item' for='instagram'>Instagram</label>

                    <div class='tab_content' id='rss_content'>
                        <div class='tab_content_description'>
                            <?php $tab->show('Rss') ?>
                        </div>
                    </div>

                    <div class='tab_content' id='youtube_content'>
                        <div class='tab_content_description'>
                            <?php $tab->show('Youtube') ?>
                        </div>
                    </div>

                    <div class='tab_content' id='twitter_content'>
                        <div class='tab_content_description'>
                            <?php $tab->show('Twitter') ?>
                        </div>
                    </div>
                    <div class='tab_content' id='instagram_content'>
                        <div class='tab_content_description'>
                            <?php $tab->show('Instagram') ?>
                        </div>
                    </div>
                </div>

                <form method='post' action='options.php' id='all_options'>
                    <?php 
                        settings_fields(ryt_gq_get_name());
                        foreach(ryt_gq_get_sections() as $section) {
                            $fetch_items = get_option($section . '_fetch_items', '');
                            $updated = get_option(ryt_gq_get_name() . '_updated' . '');
                            echo("<input type='hidden' name='" . $section . "_fetch_items' id='" . $section . "_fetch_items' value='$fetch_items'>");
                        }
                        echo("<input type='hidden' name='" . ryt_gq_get_name() . "_updated' id='" . ryt_gq_get_name() . "_updated' value='$updated'>");
                    ?>
                </form>
            </div>
            <?php
                $upload_dir = wp_upload_dir();
                $upload_url = $upload_dir['url'];
            ?>
            <div id='ryq_dialog' style='display:none;' data-upload-url='<?= $upload_url ?>'>
                <p></p>
            </div>
<?php
    }
}