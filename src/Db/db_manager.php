<?php
class RytGQDbManager {
    public function setup_db() {
        global $wpdb;

        $table_name = ryt_gq_get_history_table();
        $charset_collate = $wpdb->get_charset_collate();
        $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            time datetime DEFAULT '2001-01-01 00:00:01' NOT NULL,
            section tinytext NOT NULL,
            target_url text NOT NULL,
            UNIQUE KEY id (id)
        )
        $charset_collate;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }

    public function delete_db(){
        global $wpdb;
        $table_name = ryt_gq_get_history_table();
        $sql = "DROP TABLE IF EXISTS {$table_name}";
        $wpdb->query($sql);
    }
}
