<?php
class RytGQPostManager {
    public function __construct() {
        $this->section = '';
        $this->post_items = array();
        $this->title = '';
        $this->author = '';
        $this->category = '';
        $this->is_published = false;
    }

    public function initialize($section, $title, $options) {
        $this->section = $section;
        $this->post_items = array();
        $this->title = $title;
        $this->author = $options->user;
        $this->category = $options->cat;
        $this->is_published = $options->pub;
        $this->image_id = $options->img_id;
    }
    
    public function add($url) {
        $pi = new RytGQPostItem($this->section, $url);
        if ($pi->is_fetched()) {
            return null;
        } else {
            $this->post_items[] = $pi;
            return $pi;
        }
    }

    private function is_rss() {
        return ($this->section == 'Rss');
    }

    private function get_status() {
        return $this->is_published ? 'publish' : 'draft';
    }

    public function count() {
        return count($this->post_items);
    }

    public function items() {
        return $this->post_items;
    }
    
    public function post() {
        if ($this->count() < 1)
            return;

        if ($this->is_rss()) {
            $this->post_rss();
        } else {
            $this->post_conclusion();
        }
    }

    public function post_rss() {
        foreach($this->post_items as $item) {
            $post_body = array(
                'post_title'    => $item->title,
                'post_content'  => $this->get_content($item),
                'post_status'   => $this->get_status(),
                'post_author'   => $this->author,
                'post_category' => array($this->category)
            );
            remove_filter('content_save_pre', 'wp_filter_post_kses');
            $this->insert_post($post_body);
            $item->set_fetched();
        }
    }

    public function post_conclusion() {
        $content = '';
        foreach($this->post_items as $item) {
            $content .= $this->get_content($item);
        }

        $post_body = array(
            'post_title'    => $this->title,
            'post_content'  => $content,
            'post_status'   => $this->get_status(),
            'post_author'   => $this->author,
            'post_category' => array($this->category)
        );
        remove_filter('content_save_pre', 'wp_filter_post_kses');
        $this->insert_post($post_body);
        foreach($this->post_items as $item) {
            $item->set_fetched();
        }
    }

    private function get_content($item) {
        $class_pfx = ryt_gq_get_name();
        $title = $this->is_rss() ? "" : "<h3 class='$class_pfx-title'>$item->title</h3>";
        return "
            <div class='$class_pfx-item'>
                $title
                <div class='$class_pfx-description'>$item->description</div>
                <div class='$class_pfx-link-box'>
                    <p>このページは<span>$item->url</span>より引用しています。</p>
                    <p><input type='button' onClick='{window.open(\"$item->url\");}' class='btn__link submit' value='本家サイトで続きを見る'></p>
                    <p class='$class_pfx-ask'>削除を希望される場合はお問い合わせください。</p>
                </div>
            </div>
        ";
    }

    private function insert_post($post_body) {
        $post_id = wp_insert_post($post_body);
        if ($this->image_id) {
            set_post_thumbnail($post_id, $this->image_id);
        }
    }
}