<?php
class RytGQAdminShowOptionsTab {

    public function show($section) {
        $fetch_items = get_option($section . '_fetch_items', '');
        $fetch_names = array(
            'Rss' =>  'フィードURL',
            'Twitter' => '検索キーワード',
            'Youtube' => '検索キーワード',
            'Instagram' => '検索キーワード',
        );
        $this->holder_var = $fetch_names[$section] . 'を入力してください';
        $this->holder_title = (($section == 'Rss') ? '{{feed_title}}' : '{{keyword}}') . ' - {{date}}';
        $upload_dir = wp_upload_dir();
        $this->holder_eyecatch = $upload_dir['url'] . '/example.png';
        ?>
            <form class='<?= ryt_gq_get_name() ?>-form' data-section='<?= $section ?>'>
                <table class='form-table'>
                    <tbody>
                        <tr>
                            <th scope='row'>取得先の設定</th>
                        </tr>
                    </tbody>
                </table>

                <table class='fetch-items <?= $section ?>'>
                    <thead>
                        <tr>
                            <th class='narrow'>取得間隔</th>
                            <th><?= $fetch_names[$section] ?></th>
                            <th>記事タイトル</th>
                            <th>アイキャッチ画像</th>
                            <th class='narrow'>カテゴリー</th>
                            <th class='narrow'>ユーザー</th>
                            <th class='narrow'>公開状態</th>
                            <th class='toolbox'></th>
                        </tr>
                    </thead>
                    <tbody class='<?= $section ?>'>
                        <tr class='template'>
                            <?= $this->get_row(array()) ?>
                        </tr>
                        <? #この行を消すと機数行の色付けがずれる ?>
                        <tr class='dummy'>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php
                            $items = ($fetch_items != '') ? json_decode($fetch_items) : array();
                            if (count($items) == 0 || !isset($items[0]->val)) {
                        ?>
                            <tr>
                                <?= $this->get_row(array()) ?>
                            </tr>
                        <?php
                            } else {
                                foreach($items as $item) {
                        ?>
                            <tr>
                                <?= $this->get_row(array(
                                    'cron_interval' => $item->val->int,
                                    'var' => $item->val->var,
                                    'title' => $item->val->title,
                                    'image' => $item->val->img,
                                    'image_id' => $item->val->img_id,
                                    'category' => $item->val->cat,
                                    'post_user' => $item->val->user,
                                    'is_published' => $item->val->pub,
                                )) ?>
                            </tr>
                        <?php
                                } 
                            }
                        ?>
                    </tbody>
                </table>
                <div><span class='add-line' data-section='<?= $section ?>'></span></div>
                <div>
                    <p>記事タイトルには下記の予約語が利用できます。予約語は投稿時に自動的に日本語に変換されます。</p>
                    <ul>
                        <?php $kw = ($section == 'Rss')
                            ? array('v' => '{{feed_title}}', 't' => '取得したフィードのタイトルに変換されます。')
                            : array('v' => '{{keyword}}', 't' => '設定した検索キーワードに変換されます。')
                        ?>
                        <li><?= $kw['v'] ?> : <?= $kw['t'] ?></li>
                        <li>{{date}} : 取得した日付に変換されます。</li>
                        <li>(例) </li>
                        <li>　記事タイトルの設定 ・・・ <?= $kw['v'] ?> - {{date}}</li>
                        <li>　実際の投稿タイトル ・・・ タイトルのサンプル - <?= ryt_gq_get_current_date() ?></li>
                    </ul>
                </div>
                <button class='button button-primary tabs-submit'>設定を保存する</button>
            </form>
        <?php
    }

    private function get_row($data_set) {
        #--- data
        $cron_interval = isset($data_set['cron_interval']) ? $data_set['cron_interval'] : -1;
        $var = isset($data_set['var']) ? $data_set['var'] : '';
        $title = isset($data_set['title']) ? $data_set['title'] : $this->holder_title;
        $image = isset($data_set['image']) ? $data_set['image'] : '';
        $image_id = isset($data_set['image_id']) ? $data_set['image_id'] : '';
        $category = isset($data_set['category']) ? $data_set['category'] : '';
        $post_user = isset($data_set['post_user']) ? $data_set['post_user'] : '';
        $is_published = isset($data_set['is_published']) ? $data_set['is_published'] : '0';

        #--- Category
        $cats = get_categories(array('get' => 'all'));
        $category_tag = "
            <select class='category postform'>
                <option class='level-0' value=''>---</option>
        ";
        foreach($cats as $cat) {
            $selected = ($cat->cat_ID == $category) ? "selected='selected'" : '';
            $category_tag .= "
                <option class='level-0' value='$cat->cat_ID' $selected > $cat->cat_name</option>
            ";
        }
        $category_tag .= "
            </select>
        ";

        #--- User
        $user_tag = "
            <select class='post_user postform'>
        ";
        $users = get_users(array('get' => 'all'));
        foreach($users as $user) {
            $user_data = $user->data;
            $selected = ($user_data->ID == $post_user) ? "selected='selected'" : '';
            $user_tag .= "
                <option class='level-0' value='$user_data->ID' $selected > $user_data->display_name</option>
            ";
        }
        $user_tag .= "
            </select>
        ";

        #--- Row data
        $render = "
            <td class='ryq_center'>
                <select class='cron_interval postform'>
        ";
        $render .= "<option class='level-0' value='-1' " . ($cron_interval == -1 ? "selected='selected'" : '') . ">停止</option>";
        $render .= "<option class='level-0' value='10min' " . ($cron_interval == '10min' ? "selected='selected'" : '') . ">10分</option>";
        $render .= "<option class='level-0' value='hourly' " . ($cron_interval == 'hourly' ? "selected='selected'" : '') . ">60分</option>";
        $render .= "<option class='level-0' value='daily' " . ($cron_interval == 'daily' ? "selected='selected'" : '') . ">1日</option>";
        $render .= "
                </select>
            </td>
            <td><input type='text' class='editbox' value='$var' title='$var' placeholder='$this->holder_var'></td>
            <td><input type='text' class='editbox' value='$title' title='$title' placeholder='$this->holder_title'></td>
            <td><input type='text' class='editbox select-eyecatch' value='$image' title='$image' placeholder='$this->holder_eyecatch' data-media-id='$image_id'></td>
            <td class='ryq_center'>$category_tag</td>
            <td class='ryq_center'>$user_tag</td>
            <td class='ryq_center'>
                <select class='is_published postform'>
        ";
        $render .= "<option class='level-0' value='0' " . ($is_published == '0' ? "selected='selected'" : '') . ">下書き</option>";
        $render .= "<option class='level-0' value='1' " . ($is_published == '1' ? "selected='selected'" : '') . ">公開</option>";
        $render .= "
                </select>
            </td>
            <td class='toolbox'><span class='remove'></span></td>
        ";
        
        #---
        return $render;
    }
}