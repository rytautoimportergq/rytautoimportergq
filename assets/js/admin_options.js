jQuery(function ($) {
  var tabId = 'rss';
  var mediaTarget = null;

  var onRemove = function (e) {
    if (confirm('削除しますか？')) {
      var row = $(e.target).parent().parent();
      row.remove();
    }
  };

  var onImageClick = function (e) {
    e.preventDefault();
    mediaTarget = $(e.target);
    mediaLoader.open();
  };

  var onEditChange = function (e) {
    $(e.target).attr('title', $(e.target).val());
  };
  
  var getFetchItems = function (section) {
      //--- fetch items
      var fetchItems = [];
      ($('.' + 'fetch-items.' + section + ' tbody tr')).each(function(ix, item) {
        if (!$(item).hasClass('template') && !$(item).hasClass('dummy')) {
          //category
          var category = $(item).find('.category').val()
          //user
          var postUser = $(item).find('.post_user').val()
          //cron_interval
          var cronInterval = $(item).find('.cron_interval').val()
          //cron_interval
          var isPublished = $(item).find('.is_published').val()
          //var, title
          var box = $(item).find('.editbox');
          if ($(box[0]).val().trim() != '' && $(box[1]).val().trim() != '') {
            var hash = sha256($(box[0]).val());
            fetchItems.push({
              'key': hash,
              'val': {
                'var': $(box[0]).val(),
                'title': $(box[1]).val(),
                'img': $(box[2]).val(),
                'img_id': $(box[2]).attr('data-media-id'),
                'cat': category,
                'user': postUser,
                'int': cronInterval,
                'pub': isPublished
              }
            })
          }
        }
      });
      return JSON.stringify(fetchItems)
  };

  var mediaLoader = wp.media({
    title: 'アイキャッチ画像を選択する',
    library: {
      type: 'image'
    },
    button: {
      text: '選択する'
    },
    multiple: false
  });
  mediaLoader.on('select', function () {
    var images = mediaLoader.state().get('selection');
    images.each(function(file) {
      var info = file.toJSON();
      mediaTarget.val(info.url);
      mediaTarget.attr('title', mediaTarget.val());
      mediaTarget.attr('data-media-id', info.id);
    });
  });

  $(document).ready(function () {
    $('.fetch-items .template').hide();
    $('.fetch-items .dummy').hide();
    $('.fetch-items .remove').click(onRemove);

    $('.add-line').click(function (e) {
      var section = $(e.target).attr('data-section');
      var tbody = $('.fetch-items .' + section);
      var template = tbody.find('.template');
      var newRow = template.clone();
      newRow.find('.remove').click(onRemove);
      newRow.find('.select-eyecatch').click(onImageClick);
      newRow.find('.editbox').on('change', onEditChange);
      newRow.removeClass('template').show();
      tbody.append(newRow);
    });

    $('.select-eyecatch').click(onImageClick);

    $('.editbox').on('change', onEditChange);

    $('.ryt-autoimporter-gq-form .tabs-submit').click(function(e) {
      e.preventDefault();
      //--- save all tabs
      var updated = {};
      $('.ryt-autoimporter-gq-form').each(function(index, element){
        var section = $(element).attr('data-section');
        var saved = $('#' + section + '_fetch_items').val();
        var oldVal = '';
        if (saved != '') {
          oldVal = JSON.stringify(JSON.parse(saved));
        }
        if (oldVal != getFetchItems(section)) {
          $('#' + section + '_fetch_items').val(getFetchItems(section));
          updated[section] = '1';
        }
      });
      $('#ryt-autoimporter-gq_updated').val(JSON.stringify({'time': Date.now(), 'value': updated}));
      $('#all_options').submit();
    });
  });
});