<?php
class RytGQPostItem {
    public $section;
    public $url;
    public $title;
    public $description;

    public function __construct($section, $url) {
        $this->section = $section;
        $this->url = $url;
    }

    public function is_fetched() {
        global $wpdb;
        $table_name = ryt_gq_get_history_table();

        $sql = "SELECT id FROM {$table_name} WHERE section = '$this->section' AND target_url = '$this->url';";
        $results = $wpdb->get_results($sql);

        return count($results) != 0;
    }

    public function set_fetched() {
        global $wpdb;
        $table_name = ryt_gq_get_history_table();

        $t = current_time('mysql');
        $sql = "INSERT INTO {$table_name} (time,section,target_url) VALUES('$t','$this->section','$this->url');";
        $wpdb->query($sql);
    }
}