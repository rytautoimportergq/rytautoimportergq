<?php
class RytGQFilters {
    public function __construct() {
        $this->add_filters();
    }

    private function add_filters() {
        add_filter('plugin_action_links', array(ryt_gq_get_instance('AdminMenu'), 'plugin_action_links'), 10, 2);
        add_filter('cron_schedules', array(ryt_gq_get_instance('Cron'), 'cron_schedules'));
        add_filter('script_loader_tag', array(ryt_gq_get_instance('PostAppearance'), 'addasync_enqueue_script'), 10, 2);
    }
}
