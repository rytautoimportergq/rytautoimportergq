<?php
class RytGQInstagram {
    public function __construct() {
        $this->section = 'Instagram';
    }

    public function scheduled_action($key) {
        $pm = ryt_gq_get_instance('PostManager');

        $fetch_items = get_option($this->section . '_fetch_items', '');
        $fetch_items = ($fetch_items != '') ? json_decode($fetch_items, true) : array();

        foreach ($fetch_items as $item) {
            if ($item['key'] === $key) {
                $fetch_item = (object)$item['val'];

                // HTTP Request
                $tag = $fetch_item->var;
                $insta_url = "https://www.instagram.com/explore/tags/$tag/?__a=1";
                $curl = curl_init($insta_url);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                $response = json_decode(curl_exec($curl));
                curl_close($curl);

                $title = $fetch_item->title;
                $title = str_replace('{{keyword}}', $tag, $title);
                $title = str_replace('{{date}}', ryt_gq_get_current_date(), $title);
        
                $pm->initialize($this->section, $title, $fetch_item);

                foreach($response->graphql->hashtag->edge_hashtag_to_top_posts->edges as $result) {
                    if ($pm->count() > 9) {
                        break;
                    }
                    $node = $result->node;
                    $username = $this->get_username_by_id($node->owner->id);
                    $url = "https://www.instagram.com/p/$node->shortcode";
                    $post_item = $pm->add($url);
                    if (!is_null($post_item)) {
                        $text = $node->edge_media_to_caption->edges[0]->node->text;
                        $post_item->title =  explode("\n", mb_substr(strip_tags($text), 0, 15))[0];
                        $post_item->description = "
                            <blockquote class='instagram-media' data-instgrm-captioned 
                                data-instgrm-permalink='$url'
                                data-instgrm-version='8'
                                style='background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);'
                            >
                                <div style='padding:8px;'>
                                    <div style='background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;'>
                                        <div style='background:url($node->display_url); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;'>
                                        </div>
                                    </div>
                                    <p style='margin:8px 0 0 0; padding:0 4px;'>
                                        <a href='$url' style='color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;' target='_blank'>
                                            $text
                                        </a>
                                    </p>
                                </div>
                            </blockquote>
                        ";
                    }
                }
                $pm->post();
                return;
            }
        }
    }

    private function get_username_by_id($userid) {
        $uid_url = "https://i.instagram.com/api/v1/users/$userid/info/";
        $curl = curl_init($uid_url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = json_decode(curl_exec($curl));

        return $response->user->full_name;
    }
}
