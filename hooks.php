<?php
class RytGQHooks {
    public function __construct() {
        $this->add_hooks();
    }

    private function add_hooks() {
        register_activation_hook(ryt_gq_get_plugin_file(), array(ryt_gq_get_instance('Installer'), 'install'));
        register_deactivation_hook(ryt_gq_get_plugin_file(), array(ryt_gq_get_instance('Installer'), 'clear_settings'));
        register_uninstall_hook (ryt_gq_get_plugin_file(), array(ryt_gq_get_instance('Installer'), 'uninstall'));
    }
}
