<?php
class RytGQRss {
    public function __construct() {
        $this->section = 'Rss';
    }

    public function scheduled_action($key) {
        $feeder = new RytGQRssFeed;
        $pm = ryt_gq_get_instance('PostManager');

        $fetch_items = get_option($this->section . '_fetch_items', '');
        $fetch_items = ($fetch_items != '') ? json_decode($fetch_items, true) : array();

        foreach ($fetch_items as $fetch_item) {
            if ($fetch_item['key'] === $key) {
                $settings = (object)$fetch_item['val'];

                $pm->initialize($this->section, $title, $settings);
                $url = $settings->var;
                include_once(ABSPATH . WPINC . '/feed.php');
                $feed = fetch_feed($url);
                if (!is_wp_error($feed)){
                    $maxitems = $feed->get_item_quantity(10);
                    $rss_items = $feed->get_items(0, $maxitems);
                    foreach($rss_items as $key => $rss_item){
                        $post_item = $pm->add($rss_item->get_permalink());
                        $title = $settings->title;
                        $title = str_replace('{{feed_title}}', $rss_item->get_title(), $title);
                        $title = str_replace('{{date}}', ryt_gq_get_current_date(), $title);
                        if (!is_null($post_item)) {
                            $post_item->title = $title;
                            $post_item->description = mb_substr(strip_tags($rss_item->get_content(), '<br>'), 0, 255);
                        }
                    }
                    $pm->post();
                } else {
                    $rss = $feeder->loadRss($url);
                    foreach($rss->item as $rss_item) {
                        $title = $settings->title;
                        $title = str_replace('{{feed_title}}', $rss_item->title, $title);
                        $title = str_replace('{{date}}', ryt_gq_get_current_date(), $title);
                        if (!is_null($post_item)) {
                            $post_item->title = $title;
                            $post_item->description = $rss_item->description;
                        }
                    }
                    $pm->post();
                }

                return;
            }
        }
    }

	function parse_rss($item) {

		global $rss_post_importer;

		// get the saved template
		$post_template = $rss_post_importer->options['settings']['post_template'];

		// get the content
		$c = $item->get_content() != "" ? $item->get_content() : $item->get_description();

		$c = apply_filters('pre_rss_pi_parse_content', $c);

		$c = $this->escape_backreference($c);

		// do all the replacements
		$parsed_content = preg_replace('/\{\$content\}/i', $c, $post_template);
		$parsed_content = preg_replace('/\{\$feed_title\}/i', $feed_title, $parsed_content);
		$parsed_content = preg_replace('/\{\$title\}/i', $item->get_title(), $parsed_content);

		// check if we need an excerpt
		$parsed_content = $this->_excerpt($parsed_content, $c);

		// strip html, if needed
		if ($strip_html == 'true') {
			$parsed_content = strip_tags($parsed_content);
		}

		$parsed_content = preg_replace('/\{\$permalink\}/i', '<a href="' . esc_url($item->get_permalink()) . '" target="_blank">' . $item->get_title() . '</a>', $parsed_content);


		$parsed_content = apply_filters('after_rss_pi_parse_content', $parsed_content);

		return $parsed_content;
    }
}